#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>


#define SEALEVELPRESSURE_HPA (1013.25)

void refresh_readings(Adafruit_BME280 bme)
{
  float f_temperature;
  float f_humidity;
  float f_pressure;
  float f_altitude;

f_temperature = bme.readTemperature();
f_humidity = bme.readHumidity();
f_pressure = bme.readPressure();
f_altitude - bme.readAltitude(SEALEVELPRESSURE_HPA);

//Temperature
Serial.print(f_temperature);
Serial.println(" C");


// Humidity
Serial.print(f_humidity);
Serial.println(" %RH");

// Altitude
Serial.print(f_altitude);
Serial.println(" m");

// Humidity
Serial.print(f_pressure);
Serial.println(" kpa");

Serial.println("-------------------------");


}
 
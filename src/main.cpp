#include <Arduino.h>
#include <SensorReadings.h>



#ifndef LED_BUILTIN
  #define LED_BUILTIN 14
#endif


Adafruit_BME280 bme;//I2C


void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.begin(9600);
  Serial.println("Serial Port Open");
  bool status;

  status = bme.begin();
  while (!status)
  {
    Serial.println("Could not find a valid BMD sensor. check wiring");
    status = bme.begin(0x76);
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delay(2000);

  }
}

void loop() {
  // put your main code here, to run repeatedly:
  refresh_readings(bme);
  digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
  delay(2000);

}

